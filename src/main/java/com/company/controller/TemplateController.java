package com.company.controller;

import com.company.annotation.ApiVersion;
import com.company.dto.TemplateDto;
import com.company.service.TemplateService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/template-project")
@ApiVersion(1)
public class TemplateController {

    private final TemplateService templateService;

    @GetMapping("/templates")
    @ApiVersion(2)
    public ResponseEntity<List<TemplateDto>> getAllTemplates() {
        return ResponseEntity.ok(templateService.findAll());
    }

    @GetMapping("/templates/{id}")
    @ApiVersion({1, 2})
    public ResponseEntity<TemplateDto> getTemplate(@PathVariable Long id) {
        return ResponseEntity.ok(templateService.findOne(id));
    }

    @PostMapping("/templates")
    public ResponseEntity<TemplateDto> createTemplate(@Valid @RequestBody TemplateDto templateDto) {
        if (templateDto.getId() != null) {
            throw new RuntimeException("A new template cannot already have an ID");
        }
        TemplateDto result = templateService.save(templateDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }

    @DeleteMapping("/templates/{id}")
    public ResponseEntity<Void> deleteTemplate(@PathVariable Long id) {
        templateService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
