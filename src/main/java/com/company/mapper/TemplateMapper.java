package com.company.mapper;

import com.company.dao.entity.TemplateEntity;
import com.company.dto.TemplateDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TemplateMapper extends EntityMapper<TemplateDto, TemplateEntity> {

}
