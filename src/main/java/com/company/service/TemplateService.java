package com.company.service;

import com.company.dao.entity.TemplateEntity;
import com.company.dao.repository.TemplateRepository;
import com.company.dto.TemplateDto;
import com.company.exception.DataNotFoundException;
import com.company.mapper.TemplateMapper;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class TemplateService {

    private final TemplateMapper templateMapper;
    private final TemplateRepository templateRepository;

    public List<TemplateDto> findAll() {
        return templateRepository.findAll().stream()
                .map(templateMapper::toDto)
                .collect(Collectors.toList());
    }

    public TemplateDto findOne(Long id) {
        return templateRepository.findById(id)
                .map(templateMapper::toDto)
                .orElseThrow(() -> DataNotFoundException.of("Data not found", id));
    }

    @Transactional
    public TemplateDto save(TemplateDto templateDto) {
        TemplateEntity templateEntity = templateMapper.toEntity(templateDto);
        templateEntity = templateRepository.save(templateEntity);
        return templateMapper.toDto(templateEntity);
    }

    @Transactional
    public void delete(Long id) {
        templateRepository.deleteById(id);
    }

}

